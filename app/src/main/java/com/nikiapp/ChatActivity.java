package com.nikiapp;

import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.nikiapp.adapter.ChatAdapter;
import com.nikiapp.dao.ChatMessage;
import com.nikiapp.dao.UserChat;
import com.nikiapp.utility.Constants;
import com.nikiapp.utility.PubNubCallBack;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sitaram on 2/21/16.
 */
public class ChatActivity extends AppCompatActivity {
    private Pubnub mPubNub;
    private EditText mMessageET;
    private ListView mListView;
    private ChatAdapter mChatAdapter;
    private SharedPreferences mSharedPrefs;
    private String username;
    private String channel;
    private ImageView sendMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);

        UserChat chat = (UserChat) getIntent().getSerializableExtra("chat");
        channel = chat.getChannel();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.icon_left_arrow_white);
        if (chat != null) {
            toolbar.setTitle(getTitle(chat));
        }
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatActivity.this.finish();
            }
        });
        this.username = mSharedPrefs.getString(Constants.CHAT_USERNAME, "Anonymous");
        this.mListView = (ListView) findViewById(R.id.list);
        this.mChatAdapter = new ChatAdapter(this, new ArrayList<ChatMessage>());
        this.mChatAdapter.userPresence(this.username, "join");
        setupAutoScroll();
        this.mListView.setAdapter(mChatAdapter);

        this.mMessageET = (EditText) findViewById(R.id.message_et);
        sendMessage = (ImageView) findViewById(R.id.sendMessage);
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        initPubNub();
    }

    private String getTitle(UserChat chat) {
        if (chat.getType().equalsIgnoreCase("group")) {
            return chat.getChannel();
        } else {
            if (chat.getPublisher().equalsIgnoreCase(username))
                return chat.getPublisher();
        }
        return chat.getSubscriber();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }


    @Override
    protected void onStop() {
        super.onStop();
        if (this.mPubNub != null)
            this.mPubNub.unsubscribeAll();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (this.mPubNub == null) {
            initPubNub();
        } else {
            subscribeWithPresence();
            history();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void initPubNub() {
        this.mPubNub = new Pubnub(Constants.PUBLISH_KEY, Constants.SUBSCRIBE_KEY);
        this.mPubNub.setUUID(this.username);
        subscribeWithPresence();
        history();
    }


    public void publish(String type, JSONObject data) {
        JSONObject json = new JSONObject();
        try {
            json.put("type", type);
            json.put("data", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.mPubNub.publish(this.channel, json, new PubNubCallBack());
    }


    public void hereNow() {
        this.mPubNub.hereNow(this.channel, new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                try {
                    JSONObject json = (JSONObject) response;
                    final JSONArray hereNowJSON = json.getJSONArray("uuids");
                    final Set<String> usersOnline = new HashSet<String>();
                    usersOnline.add(username);
                    for (int i = 0; i < hereNowJSON.length(); i++) {
                        usersOnline.add(hereNowJSON.getString(i));
                    }
                    ChatActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mChatAdapter.setOnlineNow(usersOnline);

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void setStateLogin() {
        Callback callback = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                Log.d("PUBNUB", "State: " + response.toString());
            }
        };
        try {
            JSONObject state = new JSONObject();
            state.put(Constants.STATE_LOGIN, System.currentTimeMillis());
            this.mPubNub.setState(this.channel, this.mPubNub.getUUID(), state, callback);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void subscribeWithPresence() {
        Callback subscribeCallback = new Callback() {
            @Override
            public void successCallback(String channel, Object message) {
                if (message instanceof JSONObject) {
                    try {
                        JSONObject jsonObj = (JSONObject) message;
                        JSONObject json = jsonObj.getJSONObject("data");
                        String name = json.getString(Constants.JSON_USER);
                        String msg = json.getString(Constants.JSON_MSG);
                        long time = json.getLong(Constants.JSON_TIME);
                        if (name.equals(mPubNub.getUUID())) return;
                        final ChatMessage chatMsg = new ChatMessage(name, msg, time);
                        ChatActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mChatAdapter.addMessage(chatMsg);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("PUBNUB", "Channel: " + channel + " Msg: " + message.toString());
            }

            @Override
            public void connectCallback(String channel, Object message) {
                Log.d("Subscribe", "Connected! " + message.toString());
                hereNow();
                setStateLogin();
            }
        };
        try {
            mPubNub.subscribe(this.channel, subscribeCallback);
            presenceSubscribe();
        } catch (PubnubException e) {
            e.printStackTrace();
        }
    }

    /**
     * Subscribe to presence.
     * as well as add/remove current user from the chat adapter's userPresence array.
     * This array is used to see what users are currently online and display a green dot next
     * to users who are online.
     */
    public void presenceSubscribe() {
        Callback callback = new Callback() {
            @Override
            public void successCallback(String channel, Object response) {
                Log.i("PN-pres", "Pres: " + response.toString() + " class: " + response.getClass().toString());
                if (response instanceof JSONObject) {
                    JSONObject json = (JSONObject) response;
                    Log.d("PN-main", "Presence: " + json.toString());
                    try {
                        final String user = json.getString("uuid");
                        final String action = json.getString("action");
                        ChatActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mChatAdapter.userPresence(user, action);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {
                Log.d("Presence", "Error: " + error.toString());
            }
        };
        try {
            this.mPubNub.presence(this.channel, callback);
        } catch (PubnubException e) {
            e.printStackTrace();
        }
    }


    public void history() {
        this.mPubNub.history(this.channel, 100, false, new Callback() {
            @Override
            public void successCallback(String channel, final Object message) {
                try {
                    JSONArray json = (JSONArray) message;
                    Log.d("History", json.toString());
                    final JSONArray messages = json.getJSONArray(0);
                    final List<ChatMessage> chatMsgs = new ArrayList<ChatMessage>();
                    for (int i = 0; i < messages.length(); i++) {
                        try {
                            if (!messages.getJSONObject(i).has("data")) continue;
                            JSONObject jsonMsg = messages.getJSONObject(i).getJSONObject("data");
                            String name = jsonMsg.getString(Constants.JSON_USER);
                            String msg = jsonMsg.getString(Constants.JSON_MSG);
                            long time = jsonMsg.getLong(Constants.JSON_TIME);
                            ChatMessage chatMsg = new ChatMessage(name, msg, time);
                            chatMsgs.add(chatMsg);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    ChatActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mChatAdapter.setMessages(chatMsgs);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {

            }
        });
    }

    /**
     * Setup the listview to scroll to bottom anytime it receives a message.
     */
    private void setupAutoScroll() {
        this.mChatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                mListView.setSelection(mChatAdapter.getCount() - 1);
            }
        });
    }


    public void sendMessage() {
        String message = mMessageET.getText().toString();
        if (message.equals("")) return;
        mMessageET.setText("");
        ChatMessage chatMsg = new ChatMessage(username, message, System.currentTimeMillis());
        try {
            JSONObject json = new JSONObject();
            json.put(Constants.JSON_USER, chatMsg.getUsername());
            json.put(Constants.JSON_MSG, chatMsg.getMessage());
            json.put(Constants.JSON_TIME, chatMsg.getTimeStamp());
            publish(Constants.JSON_GROUP, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mChatAdapter.addMessage(chatMsg);
    }

}

