package com.nikiapp.services;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.nikiapp.utility.Constants;
import com.nikiapp.dao.Response;

import java.util.HashMap;

/**
 * Created by sitaram on 2/21/16.
 */
public class AddUserTask extends AsyncTask<Void, Void, Response> {
    HashMap<String, String> postDataParams;
    LoginListener loginListener;

    public AddUserTask(HashMap<String, String> postDataParams, LoginListener listener) {
        this.postDataParams = postDataParams;
        this.loginListener = listener;
    }

    @Override
    protected Response doInBackground(Void... params) {
        try {
            String response = WebUtility.postDataToService(postDataParams, Constants.USER);
            if (response != null) {
                Gson gson = new Gson();
                return gson.fromJson(response, Response.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        if (response != null)
            loginListener.onLoginSuccess();
        else
            loginListener.onLoginError();
    }

    public interface LoginListener {
        void onLoginSuccess();

        void onLoginError();
    }
}
