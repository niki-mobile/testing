package com.nikiapp.services;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.nikiapp.utility.Constants;
import com.nikiapp.dao.Response;

import java.util.HashMap;

/**
 * Created by sitaram on 2/21/16.
 */
public class NewChatTask extends AsyncTask<Void, Void, Response> {

    HashMap<String, String> chatData;
    private ChatAddListener listener;

    public NewChatTask(HashMap<String, String> chatData, ChatAddListener listener) {
        this.chatData = chatData;
        this.listener = listener;

    }

    @Override
    protected Response doInBackground(Void... params) {
        try {
            String response = WebUtility.postDataToService(chatData, Constants.CHAT);
            if (response != null) {
                Gson gson = new Gson();
                return gson.fromJson(response, Response.class);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
        if (response != null) {
            listener.onSuccess(response);
        } else {
            listener.onError();
        }
    }

    public interface ChatAddListener {
        void onSuccess(Response user);

        void onError();
    }


}
