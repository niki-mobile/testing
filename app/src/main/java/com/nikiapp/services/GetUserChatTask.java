package com.nikiapp.services;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nikiapp.utility.ChatListener;
import com.nikiapp.utility.Constants;
import com.nikiapp.dao.UserChat;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by sitaram on 2/21/16.
 */
public class GetUserChatTask extends AsyncTask<Void, Void, List<UserChat>> {

    String userId;
    Context context;
    ChatListener listener;

    public GetUserChatTask(Context context, String userId, ChatListener listener) {
        this.userId = userId;
        this.context = context;
        this.listener = listener;

    }

    @Override
    protected List<UserChat> doInBackground(Void... params) {
        try {
            String chatUrl = Constants.CHAT + "?userId=" + userId;
            String response = WebUtility.getDataFromService(chatUrl);
            if (response != null) {
                Gson gson = new Gson();

                Type listType = new TypeToken<List<UserChat>>() {
                }.getType();
                List<UserChat> chatList = gson.fromJson(response,
                        listType);

                return chatList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;


    }

    @Override
    protected void onPostExecute(List<UserChat> chats) {
        super.onPostExecute(chats);
        if (chats != null && chats.size() > 0) {
            listener.onSuccess(chats);
        } else {
            listener.onError();
        }
    }

}
