package com.nikiapp.services;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.nikiapp.dao.Content;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by sitaram on 2/21/16.
 */
public class NotifyUser extends AsyncTask<Void, Void, String> {
    String apiKey = "AIzaSyDYSjEzH8pZgX6WjLZG-PU6oK7QElGzXIc";
    Content content;

    public NotifyUser(Content content) {
        this.content = content;
    }

    @Override
    protected String doInBackground(Void... params) {
        String response = "";
        try {
            Gson gson = new Gson();
            String requestData = gson.toJson(content, Content.class);

            URL url = new URL("https://android.googleapis.com/gcm/send");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(requestData);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
                Log.d("Login", response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
