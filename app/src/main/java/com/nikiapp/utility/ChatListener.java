package com.nikiapp.utility;

import com.nikiapp.dao.UserChat;

import java.util.List;

/**
 * Created by sitaram on 2/21/16.
 */
public interface ChatListener {
    void onSuccess(List<UserChat> chats);

    void onError();

}
