package com.nikiapp.utility;

public class Constants {
    public static final String PUBLISH_KEY = "pub-c-7fad26fe-6c38-4940-b9c3-fbd19a9633af";
    public static final String SUBSCRIBE_KEY = "sub-c-30c17e1a-0007-11e5-a8ef-0619f8945a4f";

    public static final String CHAT_PREFS = "SHARED_PREFS";
    public static final String CHAT_USERNAME = "SHARED_PREFS.USERNAME";
    public static final String GROUP_CHANNEL = "GroupChat";

    public static final String JSON_GROUP = "groupMessage";
    public static final String JSON_USER = "chatUser";
    public static final String JSON_MSG = "chatMsg";
    public static final String JSON_TIME = "chatTime";

    public static final String STATE_LOGIN = "loginTime";

    public static final String GCM_SENDER_ID = "446172513479";

    public static final String SERVICE_BASE = "http://54.148.79.190:8040/nikichatserver/";
    public static final String CHAT = SERVICE_BASE + "chat/";

    public static final String USER = SERVICE_BASE + "user/";


}
