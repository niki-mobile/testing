package com.nikiapp.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.nikiapp.R;

/**
 * Created by sitaram on 2/21/16.
 */
public class Utility {
    public static boolean isNetwork(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {

            Toast.makeText(ctx, ctx.getString(R.string.newtork_not_available), Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
