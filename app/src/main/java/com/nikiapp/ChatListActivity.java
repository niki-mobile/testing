package com.nikiapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.nikiapp.adapter.ChatListAdapter;
import com.nikiapp.dao.Content;
import com.nikiapp.dao.Response;
import com.nikiapp.dao.UserChat;
import com.nikiapp.services.GetUserChatTask;
import com.nikiapp.services.NewChatTask;
import com.nikiapp.services.NotifyUser;
import com.nikiapp.utility.ChatListener;
import com.nikiapp.utility.Constants;
import com.nikiapp.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sitaram on 2/21/16.
 */
public class ChatListActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView examsListView;
    private ChatListAdapter adapter;
    private SharedPreferences mSharedPrefs;
    private FloatingActionButton createNewChat;
    HashMap<String, String> chatData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        mSharedPrefs = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
        String userId = mSharedPrefs.getString(Constants.CHAT_USERNAME, null);
        if (userId != null) {
            if (Utility.isNetwork(this))
                getUserChats(userId);
        } else {
            Intent toLogin = new Intent(this, LoginActivity.class);
            startActivity(toLogin);
            this.finish();
        }
        examsListView = (RecyclerView) findViewById(R.id.chatList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        examsListView.setLayoutManager(layoutManager);

        adapter = new ChatListAdapter(this);
        examsListView.setAdapter(adapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Chats");
        setSupportActionBar(toolbar);
        createNewChat = (FloatingActionButton) findViewById(R.id.createNewChat);
        createNewChat.setOnClickListener(this);


    }

    private void getUserChats(String userId) {
        GetUserChatTask task = new GetUserChatTask(this, userId, listener);
        task.execute();
    }

    ChatListener listener = new ChatListener() {
        @Override
        public void onSuccess(List<UserChat> chats) {
            UserChat chat = new UserChat();
            chat.setChannel(Constants.GROUP_CHANNEL);
            chat.setType("group");
            chats.add(chat);
            adapter.updateChatData(chats);
        }

        @Override
        public void onError() {
            UserChat chat = new UserChat();
            chat.setChannel(Constants.GROUP_CHANNEL);
            chat.setType("group");
            List<UserChat> chats = new ArrayList<>();
            chats.add(chat);
            adapter.updateChatData(chats);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createNewChat:
                createNewChat();
                break;
            default:
                break;
        }
    }

    private void createNewChat() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.create_channe, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.editTextDialogUserInput);
        final RadioGroup radioGroup = (RadioGroup) promptsView.findViewById(R.id.radioGroupChannelSelection);
        userInput.setSelection(userInput.getText().length());

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                int type = radioGroup.getCheckedRadioButtonId();
                                createChat(userInput.getText().toString(), type);

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void createChat(String name, int type) {
        String userId = mSharedPrefs.getString(Constants.CHAT_USERNAME, null);
        chatData = new HashMap<>();
        if (type == R.id.privateChat) {
            chatData.put("type", "user");
            chatData.put("publisher", userId);
            chatData.put("subscriber", name);
            String channel = userId + name;
            chatData.put("channel", channel);
            chatData.put("userId", userId);
        } else {
            chatData.put("type", "group");
            chatData.put("publisher", null);
            chatData.put("subscriber", null);
            chatData.put("channel", name);
            chatData.put("userId", userId);
        }
        new NewChatTask(chatData, chatAddListener).execute();

    }

    NewChatTask.ChatAddListener chatAddListener = new NewChatTask.ChatAddListener() {
        @Override
        public void onSuccess(Response response) {
            UserChat userChat = new UserChat();
            userChat.setChannel(chatData.get("channel"));
            userChat.setType(chatData.get("type"));
            userChat.setPublisher(chatData.get("publisher"));
            userChat.setSubscriber(chatData.get("subscriber"));
            adapter.addNewChat(userChat);
            notifyUser(response.getDeviceId());
        }

        @Override
        public void onError() {
            Toast.makeText(ChatListActivity.this, "unable to create new chat", Toast.LENGTH_LONG).show();
        }
    };

    private void notifyUser(String deviceId) {
        Content content = createContent(deviceId);
        new NotifyUser(content).execute();
    }

    public Content createContent(String deviceId) {
        Content c = new Content();
        c.addRegId(deviceId);
        c.createData(mSharedPrefs.getString(Constants.CHAT_USERNAME, null), "New Chat request");
        return c;
    }
}
