package com.nikiapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nikiapp.ChatActivity;
import com.nikiapp.R;
import com.nikiapp.dao.UserChat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sitaram on 2/21/16.
 */
public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatViewHolder> {

    List<UserChat> chatMessageList;
    Context context;
    String userName;

    public ChatListAdapter(Context context) {
        this.chatMessageList = new ArrayList<>();
        this.context = context;

    }

    public void updateChatData(List<UserChat> chatList) {
        this.chatMessageList.addAll(chatList);
        notifyDataSetChanged();
    }

    public void addNewChat(UserChat chat) {
        this.chatMessageList.add(chat);
        notifyDataSetChanged();
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ChatViewHolder(LayoutInflater.from(context).inflate
                (R.layout.item_chat_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        final UserChat chat = chatMessageList.get(position);
        if (chat != null) {

            if (chat.getType().equalsIgnoreCase("group")) {
                holder.chatIcon.setImageResource(R.drawable.icon_group);
                holder.chatName.setText(chat.getChannel());
            } else {
                holder.chatIcon.setImageResource(R.drawable.icon_user);
                if (chat.getPublisher().equalsIgnoreCase(userName)) {
                    holder.chatName.setText(chat.getPublisher());
                } else {
                    holder.chatName.setText(chat.getSubscriber());
                }
            }
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("chat", chat);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return chatMessageList.size();
    }

    public class ChatViewHolder extends RecyclerView.ViewHolder {
        TextView chatName;
        ImageView chatIcon;
        View mView;

        public ChatViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            chatName = (TextView) itemView.findViewById(R.id.chatName);
            chatIcon = (ImageView) itemView.findViewById(R.id.chatIcon);

        }
    }
}
