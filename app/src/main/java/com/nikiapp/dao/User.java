package com.nikiapp.dao;

import java.io.Serializable;

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private String deviceId;

	public User() {
		super();

	}

	public User(String userId, String deviceId) {
		super();
		this.userId = userId;
		this.deviceId = deviceId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRegistrationId() {
		return deviceId;
	}

	public void setRegistrationId(String deviceId) {
		this.deviceId = deviceId;
	}

}
