package com.nikiapp.dao;

import java.io.Serializable;

public class UserChat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1099684102481383127L;

	private long id;
	private String channel;
	private String type;
	private String publisher;
	private String subscriber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}

}
