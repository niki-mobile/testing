package com.nikiapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nikiapp.services.AddUserTask;
import com.nikiapp.utility.Constants;
import com.nikiapp.utility.Utility;

import java.io.IOException;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mUsername;
    GoogleCloudMessaging gcm;
    String regid, username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mUsername = (EditText) findViewById(R.id.login_username);
        Button startChatBtn = (Button) findViewById(R.id.startChatBtn);
        startChatBtn.setOnClickListener(this);
        getRegId();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startChatBtn:
                if (Utility.isNetwork(this))
                    joinChat();
                break;
        }
    }

    public void joinChat() {
        username = mUsername.getText().toString();
        if (!validUsername(username))
            return;
        HashMap<String, String> postDataParams = new HashMap<String, String>();
        postDataParams.put("userId", username);
        postDataParams.put("deviceId", regid);
        new AddUserTask(postDataParams, loginListener).execute();

    }

    private boolean validUsername(String username) {
        if (username.length() == 0) {
            mUsername.setError("Username cannot be empty");
            return false;
        }
        return true;
    }

    public void getRegId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {

                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regid = gcm.register(Constants.GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    Log.i("GCM", msg);


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);
    }

    AddUserTask.LoginListener loginListener = new AddUserTask.LoginListener() {
        @Override
        public void onLoginSuccess() {
            SharedPreferences sp = getSharedPreferences(Constants.CHAT_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor edit = sp.edit();
            edit.putString(Constants.CHAT_USERNAME, username);
            edit.apply();
            Intent intent = new Intent(LoginActivity.this, ChatListActivity.class);
            startActivity(intent);
            LoginActivity.this.finish();
        }

        @Override
        public void onLoginError() {
            Toast.makeText(LoginActivity.this, "Error registering channel", Toast.LENGTH_SHORT).show();
        }
    };
}
